import Vue from "vue";
import App from "./App.vue";
import Router from "vue-router";
import * as Sentry from "@sentry/vue";
import { Integrations } from "@sentry/tracing";

Vue.use(Router);
Vue.config.productionTip = false;

const router = new Router({
  // ...
});

Sentry.init({
  Vue,
  dsn: "https://cc54b3fdc2ae44e0a1f550759d4ac1d8@o1084695.ingest.sentry.io/6094663",
  integrations: [
    new Integrations.BrowserTracing({
      routingInstrumentation: Sentry.vueRouterInstrumentation(router),
      tracingOrigins: ["localhost", "my-site-url.com", /^\//],
    }),
  ],
  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
